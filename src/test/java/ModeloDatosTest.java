import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.h2.jdbcx.JdbcConnectionPool;
import org.assertj.db.type.Table;
import java.sql.*;
import static org.assertj.db.api.Assertions.assertThat;
import static org.assertj.db.output.Outputs.output;

public class ModeloDatosTest {

    private Connection conn;
    protected static JdbcConnectionPool dataSource;

    @Test
    public void testActualizaJugador() throws SQLException {
        //Se crea la base de datos de prueba
        dataSource = JdbcConnectionPool.create("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "user", "password");
        Connection conn = dataSource.getConnection();
        Statement statement = conn.createStatement();

        //Creamos la tabla e insertamos un elemento (nombre, votos = 0)
        statement.execute("CREATE TABLE Jugadores (nombre VARCHAR(255), votos INTEGER)");
        statement.execute("INSERT INTO Jugadores (nombre, votos) VALUES ('Luka Doncic', 0)");
        // Trazas de prueba
        System.out.println("testActualizaJugador:: Prueba de ActualizaJugador");
        // Realizamos el update que realiza la función actualizarJugador() de ModeloDatos.java
        String nombre = "Luka Doncic";
        statement.execute("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");

        // Recuperamos los datos insertados en la tabla y los mostramos por consola
        Table table = new Table(dataSource, "Jugadores");
        output(table).toConsole();
        
        //Comprobamos que el voto a aumentado en uno
        assertThat(table).column("votos")
        .value().isEqualTo(1);

        // Comprobación adicional para ver que el nombre introducido es correcto
        assertThat(table).column("nombre")
        .value().isEqualTo("Luka Doncic");
    }

    
    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @AfterAll
    public void tearDown() throws SQLException {
        // limpiar la base de datos de prueba
        Statement stmt = conn.createStatement();
        stmt.execute("DROP TABLE Jugadores");
        conn.close();
        }

}
