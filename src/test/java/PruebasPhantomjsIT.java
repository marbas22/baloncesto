import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void resetVotosTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebElement resetBtn = driver.findElement(By.id("reset-votos-btn"));
        resetBtn.click();

        // Volvemos a la página principal de la página para ir a la de ver votos
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement verVotosBtn = wait.until(ExpectedConditions.elementToBeClickable(By.id("ver-votos-btn")));
        verVotosBtn.click();

        WebElement botonHome = wait.until(ExpectedConditions.elementToBeClickable(By.id("botonHome")));
        System.out.println(driver.getTitle());

        List<WebElement> rows = driver.findElements(By.id("idVotos"));
        System.out.println("Tamanio de list " + rows.size());
        for (WebElement row : rows) {
            System.out.println(row.getText());
            assertEquals("0", row.getText());
        }

        driver.close();
        driver.quit();
    }

    @Test
    public void votarOtroJugador() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");

        String nombreVoto = "Manuel Arbas";
        WebElement otrosInput = driver.findElement(By.id("Otros"));
        otrosInput.clear();
        otrosInput.sendKeys(nombreVoto);

        WebElement radioOtros = driver.findElement(By.id("radioOtros"));
        radioOtros.click();

        WebElement votar = driver.findElement(By.id("votar"));
        votar.click();

        // Volvemos a la página principal de la página para ir a la de ver votos
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement verVotosBtn = wait.until(ExpectedConditions.elementToBeClickable(By.id("ver-votos-btn")));
        verVotosBtn.click();

        WebElement botonHome = wait.until(ExpectedConditions.elementToBeClickable(By.id("botonHome")));
        System.out.println(driver.getTitle());

        List<WebElement> rows = driver.findElements(By.id("idNombre"));
        int rowPostion = 0;
        for (WebElement row : rows) {
            System.out.println(row.getText());
            System.out.println("Position: " + rows.indexOf(row));
            if(row.getText().equals(nombreVoto)){
                rowPostion = rows.indexOf(row);
            }
            else{
                System.out.println("Diferentes!: " +row.getText() + " , " + nombreVoto);
            }
        }

        List<WebElement> rowsVotos = driver.findElements(By.id("idVotos"));
        System.out.println("RowPosition : " + rowPostion);
        for (WebElement row : rowsVotos) {
            if(rowsVotos.indexOf(row) == rowPostion){
                assertEquals("1", row.getText());
            }
        }
        driver.close();
        driver.quit();
    }

}
