<%@ page import="java.util.List, java.util.ArrayList, jugadores.*" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ver Votos</title>
    <style>
        /* Estilos para la tabla */
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Votos</h1>
    <table>
        <tr>
            <th>Nombre</th>
            <th>Votos</th>
        </tr>
        <%
            List<ResultJugadores> resultados = (ArrayList<ResultJugadores>) session.getAttribute("listaVotos");
            for (ResultJugadores resultado : resultados) {
        %>
        <tr>
            <td id="idNombre"><%= resultado.getNombre() %></td>
            <td id="idVotos"><%= resultado.getVotos() %></td>
        </tr>
        <%
            }
        %>
    </table>
    <br> <a id="botonHome" href="index.html"> GO HOME!</a>
</body>
</html>