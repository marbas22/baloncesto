import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.ArrayList;
import jugadores.*;

public class Recuperar extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        ResultSet resultado = bd.selectFromDatabase();
        ArrayList<ResultJugadores> listaVotos = new ArrayList<ResultJugadores>();

        try {
            while(resultado.next()){
                ResultJugadores jugador = new ResultJugadores(resultado.getString("nombre"), resultado.getInt("votos"));
                listaVotos.add(jugador);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        s.setAttribute("listaVotos", listaVotos);
        req.getRequestDispatcher("VerVotos.jsp").forward(req,res);
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
